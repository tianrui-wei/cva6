bind mmu mmu_formal u_mmu_formal(.*);

module mmu_formal
	import ariane_pkg::*;
(
    input  logic                            clk_i,
    input  logic                            rst_ni,
    input  logic                            flush_i,
    input  logic                            enable_translation_i,
    input  logic                            en_ld_st_translation_i,   // enable virtual memory translation for load/stores
    // IF interface
    input  icache_areq_o_t                  icache_areq_i,
    output icache_areq_i_t                  icache_areq_o,
    // LSU interface
    // this is a more minimalistic interface because the actual addressing logic is handled
    // in the LSU as we distinguish load and stores, what we do here is simple address translation
    input  exception_t                      misaligned_ex_i,
    input  logic                            lsu_req_i,        // request address translation
    input  logic [riscv::VLEN-1:0]          lsu_vaddr_i,      // virtual address in
    input  logic                            lsu_is_store_i,   // the translation is requested by a store
    // if we need to walk the page table we can't grant in the same cycle
    // Cycle 0
    output logic                            lsu_dtlb_hit_o,   // sent in the same cycle as the request if translation hits in the DTLB
    output logic [riscv::PPNW-1:0]          lsu_dtlb_ppn_o,   // ppn (send same cycle as hit)
    // Cycle 1
    output logic                            lsu_valid_o,      // translation is valid
    output logic [riscv::PLEN-1:0]          lsu_paddr_o,      // translated address
    output exception_t                      lsu_exception_o,  // address translation threw an exception
    // General control signals
    input riscv::priv_lvl_t                 priv_lvl_i,
    input riscv::priv_lvl_t                 ld_st_priv_lvl_i,
    input logic                             sum_i,
    input logic                             mxr_i,
    // input logic flag_mprv_i,
    input logic [riscv::PPNW-1:0]           satp_ppn_i,
    input logic [ASID_WIDTH-1:0]            asid_i,
    input logic [ASID_WIDTH-1:0]            asid_to_be_flushed_i,
    input logic [riscv::VLEN-1:0]           vaddr_to_be_flushed_i,
    input logic                             flush_tlb_i,
    // Performance counters
    output logic                            itlb_miss_o,
    output logic                            dtlb_miss_o,
    // PTW memory interface
    input  dcache_req_o_t                   req_port_i,
    output dcache_req_i_t                   req_port_o,
    // PMP
    input  riscv::pmpcfg_t [15:0]           pmpcfg_i,
    input  logic [15:0][riscv::PLEN-3:0]    pmpaddr_i
	
);

	default clocking @(posedge clk_i); endclocking
	default disable iff (!rst_ni);

	// covers
	cover_icache_valid_req: cover property (icache_areq_i.fetch_req == 1'b1);
	cover_instruction_access_fault: cover property (icache_areq_o.fetch_exception == riscv::INSTR_ACCESS_FAULT);
	cover_instruction_page_fault: cover property (icache_areq_o.fetch_exception == riscv::INSTR_PAGE_FAULT);
	cover_instruciton_misaligned_fault: cover property (icache_areq_o.fetch_exception == riscv::INSTR_ADDR_MISALIGNED);

	cover_m_level: cover property(priv_lvl_i == riscv::PRIV_LVL_M);
	cover_s_level: cover property(priv_lvl_i == riscv::PRIV_LVL_S);
	cover_u_level: cover property(priv_lvl_i == riscv::PRIV_LVL_U);

	// assumes
	assume_disable_lsu: assume property (lsu_req_i == 1'b0);


	// don't access fault during M mode
    // assert_no_instr_access_fault_m_mode: assert property(priv_lvl_i == riscv::PRIV_LVL_M && icache_areq_o.fetch_valid |-> icache_areq_o.fetch_exception == '0 );
    assert_no_fault_m_mode: assert property(priv_lvl_i == riscv::PRIV_LVL_M && icache_areq_o.fetch_valid |-> icache_areq_o.fetch_exception.valid == 1'b0 );
    assert_no_instr_access_fault_m_mode: assert property(priv_lvl_i == riscv::PRIV_LVL_M && icache_areq_o.fetch_valid |-> !(icache_areq_o.fetch_exception.valid == 1'b1 && icache_areq_o.fetch_exception.cause == riscv::INSTR_ACCESS_FAULT) );
    assert_no_instr_page_fault_m_mode: assert property(priv_lvl_i == riscv::PRIV_LVL_M && icache_areq_o.fetch_valid |-> !( icache_areq_o.fetch_exception.valid == 1'b1 && icache_areq_o.fetch_exception.cause == riscv::INSTR_PAGE_FAULT ));


endmodule : mmu_formal