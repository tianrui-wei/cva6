analyze -sv12 \
	-f Flist.pkg

analyze -sv12 \
	-f Flist.ariane

analyze -sva mmu_formal.sv


elaborate -top mmu

clock clk_i
reset ~rst_ni

get_design_info

set_max_trace_length 20
prove -all

report
